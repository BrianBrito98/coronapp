package com.example.vmac.WatBot;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.vmac.WatBot.registro;
import com.example.vmac.WatBot.Home;

import com.example.vmac.WatBot.Api.Api;
import com.example.vmac.WatBot.Servicio.ServicioPeticion;
import com.example.vmac.WatBot.ViewModels.Peticion_Login;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private String APITOKEN = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        /*if(token != ""){
            Toast.makeText(Login.this, "token valido", Toast.LENGTH_LONG).show();
            startActivity(new Intent(Login.this, Home.class));

        }*/

        final EditText email = findViewById(R.id.editText2);
        final EditText pass = findViewById(R.id.editText);
        Button loginin = (Button) findViewById(R.id.button);

        Button button1 =  (Button) findViewById(R.id.button2);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), registro.class);
                view.getContext().startActivity(intent);}
        }
        );

        loginin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(email.getText().toString().isEmpty()||email.getText().toString()==""){
                    email.setSelectAllOnFocus(true);
                    email.requestFocus();
                    return;
                }
                if(pass.getText().toString().isEmpty()||pass.getText().toString()=="") {
                    pass.setSelectAllOnFocus(true);
                    pass.requestFocus();
                    return;
                }
                ServicioPeticion service = Api.getApi(MainActivity.this).create(ServicioPeticion.class);
                Call<Peticion_Login> loginCall = service.getLogin(email.getText().toString(),pass.getText().toString());
                loginCall.enqueue(new Callback<Peticion_Login>() {
                    @Override
                    public void onResponse(Call<Peticion_Login> call, Response<Peticion_Login> response) {
                        Peticion_Login peticion = response.body();
                        if(peticion.estado == "true"){
                            APITOKEN = peticion.token;
                            guardarPreferencias();
                            if(APITOKEN != ""){
                                startActivity(new Intent(MainActivity.this, Home.class));
                            }
                        }
                        else{
                            Toast.makeText(MainActivity.this,"Datos Incorrectos :c", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Peticion_Login> call, Throwable t) {
                        Toast.makeText(MainActivity.this,"Error", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    /*public void cargarPreferencias(){
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN","Mensaje");
    }*/

    public void guardarPreferencias(){
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = APITOKEN;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("TOKEN",token);
        editor.commit();
    }
}
