package com.example.vmac.WatBot.Servicio;

import com.example.vmac.WatBot.ViewModels.Peticion_Login;
import com.example.vmac.WatBot.ViewModels.Registro_Usuario;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ServicioPeticion {
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Registro_Usuario> signupUser(@Field("username") String correo, @Field("password") String password);

    @FormUrlEncoded
    @POST("api/login")
    Call<Peticion_Login>getLogin(@Field("username") String correo, @Field("password") String contrasenia);
}
