package com.example.vmac.WatBot.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vmac.WatBot.Home;
import com.example.vmac.WatBot.MainActivity;
import com.example.vmac.WatBot.R;
import com.example.vmac.WatBot.registro;

public class SplashActivity extends AppCompatActivity {

    //Tiempo en pantalla del Splash (2.5 segundos)
    private final int SPLASH_DELAY = 2500;

    //Imagen
    private ImageView imageView;

    //Texto
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getWindow().setBackgroundDrawable(null);

        //Metodo para llamar
        initializeView();
        animateLogo();
        gotoMainActivity();

    }

    private void initializeView() {
        imageView = findViewById(R.id.imageView);
        textView = findViewById(R.id.textView);
    }

    private void animateLogo() {
        //Este metodo va a animar el logo
        Animation fadingInAnimation  = AnimationUtils.loadAnimation(this,R.anim.fade_in);
        fadingInAnimation.setDuration(SPLASH_DELAY);

        imageView.startAnimation(fadingInAnimation);
        textView.startAnimation(fadingInAnimation);

    }

    private void gotoMainActivity() {
        //Este metodo llevara al usuario al MainActivity una vez transcurrido los 2.5s
        new Handler().postDelayed(()-> {
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
            finish();
        }, SPLASH_DELAY);
    }
}
