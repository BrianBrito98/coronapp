package com.example.vmac.WatBot;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.vmac.WatBot.Api.Api;
import com.example.vmac.WatBot.Servicio.ServicioPeticion;
import com.example.vmac.WatBot.ViewModels.Registro_Usuario;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class registro extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        Button regis = (Button) findViewById(R.id.button4);
        regis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText Usuario = (EditText) findViewById(R.id.editText1);
                EditText password = (EditText) findViewById(R.id.editText2);
                EditText rpassword = (EditText) findViewById(R.id.editText3);

                if(Usuario.getText().toString() == ""){
                    Usuario.setSelectAllOnFocus(true);
                    Usuario.requestFocus();
                    return;
                }
                if(password.getText().toString() == "") {
                    password.setSelectAllOnFocus(true);
                    password.requestFocus();
                    return;
                }
                if(rpassword.getText().toString() == "") {
                    rpassword.setSelectAllOnFocus(true);
                    rpassword.requestFocus();
                    return;
                }
                if(!password.getText().toString().equals(rpassword.getText().toString())) {
                    Toast.makeText(registro.this, "Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
                    return;
                }

                ServicioPeticion service = Api.getApi(registro.this).create(ServicioPeticion.class);
                Call<Registro_Usuario> signCall = service.signupUser(Usuario.getText().toString(),password.getText().toString());
                signCall.enqueue(new Callback<Registro_Usuario>() {
                    @Override
                    public void onResponse(Call<Registro_Usuario> call, Response<Registro_Usuario> response) {
                        Registro_Usuario petition = response.body();
                        if(response.body() == null){
                            Toast.makeText(registro.this, "Ocurrio un error, intentelo de nuevo", Toast.LENGTH_LONG).show();
                            return;
                        }
                        if(petition.estado == "true"){
                            startActivity(new Intent(registro.this, MainActivity.class));
                            Toast.makeText(registro.this, "Datos registrados con exito", Toast.LENGTH_LONG).show();
                        }
                        else{
                            Toast.makeText(registro.this, "Nombre de usuario ya en uso", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<Registro_Usuario> call, Throwable t) {
                        Toast.makeText(registro.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
